def change_spark_conf(change_list):
    try:
        conf_file = "/usr/lib/spark/conf/spark-defaults.conf"
        with open(conf_file,'r') as f:
            content = f.readlines()
            for key in change_list:
                line_number = 0
                for required_spark_conf in content:
                    if key in required_spark_conf:
                        spark_conf = required_spark_conf.split()
                        if change_list[key] not in spark_conf[1]:
                            content[line_number] = spark_conf[0]+'  '+spark_conf[1]+':'+change_list[key]+'\n'
                        else:
                            print 'Configuration: '+change_list[key]+" already present"
                        break
                    line_number += 1
        with open(conf_file, 'w') as fw:
            fw.writelines(content)
    except Exception,e:
        print ("Exception occurred while reading configuration File. Details: "+e.__str__())


def main():
    spark_conf_change_list = {"spark.driver.extraClassPath":"/home/hadoop/sqoop/lib/ojdbc6_g.jar","spark.executor.extraClassPath":"/home/hadoop/sqoop/lib/ojdbc6_g.jar"}
    change_spark_conf(spark_conf_change_list)

if __name__ == '__main__':
    main()

